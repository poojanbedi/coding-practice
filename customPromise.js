'use strict'

function customPromise(fn) {
  var _thisRef = this;
  let thenCallbacks = [], catchCallback;
  
  if(typeof fn === 'function') {
    fn.call(_thisRef, function(data) { 
      while(thenCallbacks.length > 0) {
        thenCallbacks[0].call(null, data);
        thenCallbacks.shift();
      }
    }, function(err) {
      catchCallback.call(null, err);      
    })
  }
  
  this.then = function(callback) {
    thenCallbacks.push(callback);
    return _thisRef;
  };
  this.catch = function(callback) {
    catchCallback = callback;
    return _thisRef;
  };
  return _thisRef;
}

new customPromise(function(fulfill, reject) {

  setTimeout(function() {
    if (Math.random() >= 0.5) {
      reject(new Error('Promise Rejected'))
    } else {
      fulfill('Hello World')
    }
  }, 100)

}).then(function(data) {
  console.log(data)
}).then(function(data) {
  console.log(data + '3')
}).catch(function(err) {
  console.log(err)
});