const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
const fs = require('fs');
const resolve = require('path').resolve;

let url = "mongodb://127.0.0.1:27017"

const client = new MongoClient(url,{ useNewUrlParser: true });

client.connect(function(err, client) {
  if(!assert.equal(null, err)) {
    const db = client.db('imdb');
    let isFirstNewLinePassed = false;
    let currentCellIndex = 0;
    
    let _keys = [];
    let _document = {};
    let _documentCollection = [];
    let chunkChanged = false;
    let stream = fs.createReadStream(resolve(`${__dirname}/sampleData/name.basics.tsv`))
      .setEncoding('utf8')
      .on('readable', () => {
        let chunk;
        
        async function churnChunk(chunk) {
          chunkChanged = true;
          let _chunk = chunk.split(/\t/); 
          for(let index = 0, len = _chunk.length; index < len; index += 1) {
            let newLineSplit = _chunk[index].split(/\n/);
            if(isFirstNewLinePassed === false) {
              chunkChanged = false;
              _keys.push(newLineSplit[0]);
            } else {
              let _currentKey = _keys[currentCellIndex];
              let moveToNext = false;
              if( currentCellIndex !== 0 && _document.hasOwnProperty( _keys[ currentCellIndex - 1 ] ) && chunkChanged === true ) {
                chunkChanged = false;
                _document[_keys[currentCellIndex-1]] = `${_document[_keys[currentCellIndex-1]]}${newLineSplit[0]}`;
                moveToNext = true;
              } 
              if(_currentKey !== undefined && moveToNext === false) {
                currentCellIndex += 1;
                _document[_currentKey] = newLineSplit[0];
              }
            }
            if(newLineSplit.length > 1) {
              // Determined the scope of how many cells it will have. 
              if(isFirstNewLinePassed === false){
                let _currentKey = _keys[currentCellIndex];
                isFirstNewLinePassed = true; 
                if(_currentKey !== undefined) {
                  currentCellIndex += 1;
                  _document[_currentKey] = newLineSplit[1];
                }
              } else {
                _documentCollection.push( { insertOne: { document: _document } } );

                _document = {};
                currentCellIndex = 0;
                let _currentKey = _keys[currentCellIndex];
                if(_currentKey !== undefined)
                  _document[_currentKey] = newLineSplit[1];
                  currentCellIndex += 1
              }

            }
            
          }
          let _docLength = _documentCollection.length;
          await db.collection("name1").bulkWrite(_documentCollection)
          .then( function(res) { 
            console.log(`${_docLength} document inserted`);
          } )
          .catch(() => {
            if (err) throw err;
          });
          _documentCollection = [];
        }

        while( null !== ( chunk = stream.read() ) ) {
          churnChunk(chunk);
        }
      })
      .on('error', (err) => {
        console.log('Error while reading file.', err);
      }).on('end', function(){
        console.log('Read entire file.')
        client.close();
      });

  } else {
    console.log(err);
  }
});