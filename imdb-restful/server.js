const express = require('express');
const bodyParser = require('body-parser');

const app = express(),
  port = process.env.PORT || 3000;

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

// parse requests of content-type - application/json
app.use(bodyParser.json())

const dbConfig = require('./app/config/imdb.config.js');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

mongoose.connect(dbConfig.url, {
  useNewUrlParser: true
}).then(() => {
  console.log("Successfully connected to the database");    
}).catch(err => {
  console.log('Could not connect to the database. Exiting now...', err);
  process.exit();
});

require('./app/routes/imdb.routes.js')(app);

app.listen(port);

// const MongoClient = require('mongodb').MongoClient;


// const assert = require('assert');



// const url = "mongodb://127.0.0.1:27017";
// const db_name = 'imdb';




// console.log('IMDB restful APIs is serving on: ' + port);

// const client = new MongoClient(url);