module.exports = (app) => {
    const names = require('../controllers/name.controller.js');
    const app_path = '/api';

    // function connectMongo(callback) {
    //     client.connect(function(err, client) {
    //       if(!assert.equal(null, err)) {
    //         const dbInstance = client.db( db_name );
    //         if(typeof callback === 'function') {
    //           callback(client, dbInstance);
    //         }
    //       }
    //     }, { useNewUrlParser: true });
    //   }
      
      app.get(`${app_path}/get/names`, names.getAllNames); 
      // (req, res) => {
          
      //   connectMongo( async (client, dbInstance) => {
      //     let _result = {};
      //     let collectionObj = await dbInstance.collection("name.basics").find({}, {projection: {_id: 0}}).limit( maxRecordThreshold );
      //     collectionObj.toArray(function(err, result) {
      //       if (err) throw err;
      //       _result.names = result;
      //       client.close();
      //       return res.send(_result);
      //     });
      //   } );
      // });
      
      app.get(`${app_path}/get/name/:nameConstant`, names.findByNameConstant);
      
      // app.get(`${app_path}/get/name/by/profession/:profession`, (req, res) => {
      //   connectMongo( (client, dbInstance) => {
      //   dbInstance.collection("name.basics").find({primaryProfession: new RegExp(`,{0,1}${req.params.profession},{0,1}`)}, {projection: {_id: 0}}).limit( maxRecordThreshold ).toArray(function(err, result) {
      //       if (err) throw err;
      //       client.close();
      //       return res.send({ name: result });
      //     });
      //   });
      // });
};