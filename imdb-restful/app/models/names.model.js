const mongoose = require('mongoose');
const NameClass = require('./name.class.js');
const nameSchema = new mongoose.Schema({
    nconst: String,
    primaryName: String,
    primaryProfession: String,
    knownForTitles: String
});

// const namesSchema = new mongoose.Schema({
//     names: [ nameSchema ]
// }, { _id: false, autoIndex: false })

// class NameClass {
//     static findByNameConstant(nameConstant) {
//         return this.find({nconst: nameConstant});
//     }
// }

nameSchema.loadClass(NameClass);
module.exports = mongoose.model('name.basics', nameSchema);