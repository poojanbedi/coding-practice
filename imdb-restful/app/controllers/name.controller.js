const Names = require('../models/names.model.js');
const maxRecordThreshold = 100;

function sendErrorResponse(res, err) {
    res.status(500).send({
        message: err.message || "Some error occurred while retrieving names."
    });
}

exports.create = (req, res) => {

};

exports.findByNameConstant = (req, res) => {
    Names.findByNameConstant(req.params.nameConstant).limit(maxRecordThreshold).then( names => {
        res.send(names);
    } ).catch( err => {
        sendErrorResponse(res, err);
    } );
};

// Retrieve and return all notes from the database.
exports.getAllNames = (req, res) => {
    Names.find().limit(maxRecordThreshold)
    .then( names => {
        res.send(names);
    }).catch( err => {
        sendErrorResponse(res, err);
    });
};

// Find a single note with a noteId
exports.findOne = (req, res) => {

};

// Update a note identified by the noteId in the request
exports.update = (req, res) => {

};

// Delete a note with the specified noteId in the request
exports.delete = (req, res) => {

};